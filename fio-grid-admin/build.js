const fs = require('fs-extra');
const concat = require('concat');
const Terser = require("terser");
const path = require("path");

(async function build() {
  const files = [
    './dist/fio-grid-admin/runtime.js',
    './dist/fio-grid-admin/polyfills.js',
    './dist/fio-grid-admin/scripts.js',
    './dist/fio-grid-admin/vendor.js',
    './dist/fio-grid-admin/main.js'
  ];

  await fs.ensureDir('src/web-component-build');
  concat(files).then(source => {
    const dstPath = `src/web-component-build`
    const buildFile = `fio-grid-admin-${fs.readJsonSync("./package.json").version}.js`
    const ofbizDstPath = `/home/justin/Documents/fio/ofbiz-release15.12/hot-deploy/ab-ag-grid-support/webapp`
    fs.writeFileSync(path.join(dstPath, buildFile), Terser.minify(source).code, "utf8");
    fs.copyFileSync(path.join(dstPath, buildFile), path.join(ofbizDstPath, 'assets', buildFile))
    console.log("copied wc to ofbiz dest")
  });
})();
