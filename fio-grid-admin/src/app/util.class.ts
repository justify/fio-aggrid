import * as _ from "lodash";
import * as sjs from "serialize-javascript";

export class UtilStatic {
  serializeForPOST(obj, omit?: string[]) {
    if (omit) {
      obj = _.omit(obj, omit);
    }
    let str = [],
      p;
    for (p in obj) {
      let k = p,
        v = obj[p];
      if (_.isObject(v)) {
        v = JSON.stringify(v);
      }
      str.push(encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
    return str.join("&");
  }

  serializeJS(jsObj: any): string {
    return sjs(jsObj);
  }
  
  deserializeJS(serializedJavascript): any {
    return eval("(" + serializedJavascript + ")");
  }
}

export const Util = new UtilStatic();
