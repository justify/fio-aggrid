export interface GridUserPreferences {
  instanceId: string;
  userId: string;
  role: "USER" | "ADMIN";
  description: string;
  mode: string;
  name: string;
  appliedRequestUrl: string;
  appliedRequestVerb: string;
  appliedRequestBodyJSON: string;
  datacreate: string;
  dataupdate: string;
  dataremove: string;
  gridOptionsJsString: string;
}

export interface LocalGridUserPreferences extends GridUserPreferences{
 gridOptions: FioGridOptions;
 requestBody: any;
}

//can't import AgGrid GridOptions without the whole library which is not needed here
export interface GridOptions {
  columnDefs: any[];
}
export interface FioGridOptions extends GridOptions {
  custom?: CustomGridOptions;
}

export interface CustomGridOptions {
  filterModel?: any;
  dataUniqueIdField?: string;
  rowSelection?: "single" | "multiple";
}
