import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
   <app-admin-ui [debug]="true" [allendpoint]="allEndpoint" [saveendpoint]="saveEndpoint" [removeendpoint]="removeEndpoint" [listrefreshinterval]="listrefreshinterval"></app-admin-ui>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  allEndpoint = "http://localhost:8080/ab-ag-grid-support/control/allGridUserConfig";
  saveEndpoint = "http://localhost:8080/ab-ag-grid-support/control/adminSaveGrid";
  removeEndpoint = "http://localhost:8080/ab-ag-grid-support/control/adminRemoveGrid"
  listrefreshinterval = 120000;

}
