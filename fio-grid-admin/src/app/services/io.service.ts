import { Injectable } from "@angular/core";
import * as _ from "lodash";
import { Util } from "../util.class";
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { BehaviorSubject, Observable, of } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { ToastMessage } from "../components/admin-ui/toast-message.component";
import { GridUserPreferences, LocalGridUserPreferences } from "../models/models";
import { LoggerFactory } from '../logger-factory';

export interface Config {
  allEndpoint: string;
  saveEnpoint: string;
  removeEndpoint: string;
  userId: string;
  listrefreshinterval: number;
}

@Injectable({
  providedIn: "root"
})
export class IoService {
  private logger = LoggerFactory.getLogger(this)
  
  config: Config;
  toastSub$ = new BehaviorSubject<ToastMessage>(null);
  spinSub$ = new BehaviorSubject<boolean>(false);
  constructor(private http: HttpClient) {}

  setConfig(config: Config) {
    this.config = config;
  }

  save(current: LocalGridUserPreferences) {
    this.logger.trace("GridUserPreferences to save (before serializeJS): ", current);
    current.gridOptions.columnDefs.map((coldDef, index) => {
      Object.keys(coldDef).map(k => {
        if(k.startsWith('_')){
          _.unset(current.gridOptions.columnDefs, `[${index}].${k}`);
        }
      })
    })
    current.gridOptionsJsString = Util.serializeJS(current.gridOptions)
    //detect if this is an ofbiz find service type request and add required if there are no conditions
    if(!_.isEmpty(current.requestBody) && (current.requestBody.entityName && current.requestBody.inputFields && _.isEmpty(current.requestBody.inputFields))){
      current.requestBody.noConditionFind = "Y";
    }else if(!_.isEmpty(current.requestBody)){
      delete current.requestBody.noConditionFind;
    }
    current.appliedRequestBodyJSON = JSON.stringify(current.requestBody);
    
    let merged: LocalGridUserPreferences = _.cloneDeep(current);
    delete merged.gridOptions
    delete merged.requestBody
    this.logger.info("GridUserPreferences to save: ", JSON.stringify(merged, null, 1));
    this.http
      .post(
        `${this.config.saveEnpoint}`,
        _.omit(merged, ["createdStamp", "createdTxStamp", "lastUpdatedStamp", "lastUpdatedTxStamp"]),
        {
          headers: new HttpHeaders()
            .set("Content-Type", "ofbiz-bypass/json")
        }
      )
      .subscribe((res: any) => {
        this.logger.debug("save res: ", res);
        this.toastSub$.next({ type: "success", message: "saved!" });
      });
  }

  getAll(role?: string): Observable<LocalGridUserPreferences[]> {
    let qp = role ? `?role=${role.toLocaleUpperCase()}` : "";
    return this.http.post<any[]>(`${this.config.allEndpoint}${qp}`, {}).pipe(map((prefs: GridUserPreferences[]) => {
      this.logger.trace("getAll: ", prefs)
      if(prefs){
        prefs.map((p: any) => {
          p.gridOptions = Util.deserializeJS(p.gridOptionsJsString);
          this.logger.trace("p.appliedRequestBodyJSON: ", p.appliedRequestBodyJSON)
          try{
            p.requestBody = p.appliedRequestBodyJSON? JSON.parse(p.appliedRequestBodyJSON): {};
          }catch(err){
            console.warn("parsing appliedRequestBodyJSON: ", p.appliedRequestBodyJSON, " failed setting requestBody to {}. err: ", err.message)
            p.requestBody = {};
          }
        })
      }
      this.logger.trace("getAll: LocalGridUserPreferences[]: ", prefs)
      return <LocalGridUserPreferences[]>prefs || [];
    }))
  }



  remove(remove: any): Observable<boolean> {
    let req = _.pick(remove, ["userId", "instanceId", "role"]);
    this.logger.trace("to remove: ", req);
    return this.http.post(`${this.config.removeEndpoint}?${Util.serializeForPOST(req)}`, {}).pipe(
      map((res: any) => {
        this.logger.trace("remove res: ", res);
        this.toastSub$.next({ type: "success", message: "removed!" });
        return true;
      })
    );
  }

  getSampleData(requestUrl: string, requestVerb: string, requestBody: string | any): Observable<any> {
    const isOfbizPerformFindList = requestUrl.endsWith("performFindList")
    let parseFailed = false;
    let reqBodyObj: any = {};
    if (_.isObject(requestBody)) {
      reqBodyObj = requestBody;
    } else if (_.isString(requestBody)) {
      try {
        reqBodyObj = JSON.parse(requestBody);
      } catch (err) {
        this.toastSub$.next({ type: "danger", message: "requestBody json is incorrectly formatted (eg: property names, are not in quotes), aborting request" });
        parseFailed = true;
      }
    }
    if (parseFailed) {
      return of();
    }
    this.logger.trace("getSampleData: args:", arguments, ", reqBodyObj: ", reqBodyObj);
    if (isOfbizPerformFindList) {
      console.warn("special handling for ofbiz performFindList");
      //add noConditionFind if no inputFields provided
      if(_.isEmpty(reqBodyObj.inputFields)){
        reqBodyObj.inputFields = "{}";
        reqBodyObj.noConditionFind = "Y";
      }
      reqBodyObj.startRow = 1;
      reqBodyObj.endRow = 2;
    }
    this.spinSub$.next(true);
    this.logger.trace("calling getSampleData with reqBodyObj: ", JSON.stringify(reqBodyObj, null, 2))
    return this.http[requestVerb](requestUrl, reqBodyObj).pipe(
      map((res: any) => {
        this.logger.trace("getSampleData: res: ", res);
        let entity = _.first(res);
        if (isOfbizPerformFindList) {
          console.warn("special handling for ofbiz performFindList");
          entity = _.first(res.list);
        }
        this.spinSub$.next(false);
        if (_.isEmpty(entity)) {
          this.toastSub$.next({
            type: "danger",
            message: "unable to retrieve sample data (empty response), check url, verb and body are correct!"
          });
        }
        return entity;
      }),
      catchError((err: HttpErrorResponse) => {
        this.logger.trace("err: ", err);
        this.toastSub$.next({ type: "danger", message: "unable to retrieve sample data, check url, verb and body are correct!" });
        return of();
      })
    );
  }


}
