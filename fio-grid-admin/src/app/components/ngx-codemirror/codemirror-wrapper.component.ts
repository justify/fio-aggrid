import { Input, Output, EventEmitter, ViewChild, Component } from "@angular/core";
import { FioGridOptions } from "src/app/models/models";
import { Util } from "src/app/util.class";
import { CodemirrorComponent } from "./codemirror.component";
import { LoggerFactory } from 'src/app/logger-factory';

@Component({
  selector: "codemirror-wrapper",
  template: `
    <ngx-codemirror *ngIf="showEditor" (change)="onCodeChanged($event)" [(ngModel)]="gridOptionsJsString"></ngx-codemirror>
  `,
  styles: []
})
export class CodemirrorWrapper {
  private logger = LoggerFactory.getLogger(this)
  
  @ViewChild(CodemirrorComponent) code: CodemirrorComponent;

  @Input()
  showEditor: boolean;

  gridOptionsJsString: string;
  @Input()
  set gridOptions(obj: FioGridOptions) {
    this.gridOptionsJsString = Util.serializeJS(obj);
  }

  @Output()
  codeChanged = new EventEmitter<FioGridOptions>();

  onCodeChanged($event) {
    const fioGridOptions = Util.deserializeJS(this.gridOptionsJsString);
    this.logger.trace("codemirror-wrapper: onCodeChanged. ", fioGridOptions); 
    this.codeChanged.emit(fioGridOptions);
  }
}
