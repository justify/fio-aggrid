import {
  Component,
  Input,
  Output,
  ElementRef,
  ViewChild,
  EventEmitter,
  forwardRef,
  AfterViewInit,
  OnDestroy,
  NgZone,
  ApplicationRef,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import * as CodeMirror from 'codemirror';
import 'codemirror/mode/javascript/javascript'
import 'codemirror/addon/display/autorefresh'
import 'codemirror/addon/lint/javascript-lint'

/**
 * CodeMirror component
 *
 * **Usage** :
 * ```html
 *   <ngx-codemirror [(ngModel)]="data" [config]="{...}" (init)="onInit" (blur)="onBlur" (focus)="onFocus" ...></ngx-codemirror>
 * ```
 */
@Component({
  selector: 'ngx-codemirror',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CodemirrorComponent),
      multi: true
    }
  ],
  template: '<textarea #host></textarea>',

})
export class CodemirrorComponent implements AfterViewInit, OnDestroy {


  config = {
    mode: "javascript",
    smartIndent: true,
    gutters: ['CodeMirror-lint-markers'],
    lineNumbers: true,
    lint: true,
    autoCloseBrackets: true,
    autoRefresh: true
  }

  @Output() public change = new EventEmitter();

  @Output() public focus = new EventEmitter();

  @Output() public blur = new EventEmitter();

  @Output() public cursorActivity = new EventEmitter();

  @ViewChild('host') public host: ElementRef;
  instance$ = new BehaviorSubject<any>(null);


  /** Current editor instance */
  _instance: any;

  /** Value storage */
  private _value = 'test';

  /**
   * Constructor
   *
   * @param _zone NgZone injected for Initialization
   */
  constructor(
    private changeRef: ApplicationRef,
    private _zone: NgZone
  ) { }

  /** Implements ControlValueAccessor.value */
  get value() { return this._value; }

  /** Implements ControlValueAccessor.value */
  @Input() set value(v) {
    if (v !== this._value) {
      this._value = v;
      this.onChange(v);


    }
  }

  /**
   * On component destroy
   */
  public ngOnDestroy() {

  }

  /**
   * On component view init
   */
  public ngAfterViewInit() {
    this.codemirrorInit(this.config);
  }

  /**
   * Value update process
   */
  public updateValue(value: any) {
    this.value = value;
    this.onTouched();
    this.change.emit(value);
  }

  /**
   * Implements ControlValueAccessor
   */
  public writeValue(value: any) {
    this._value = value || '';
    if (this._instance) {
      this._instance.setValue(this._value);
    }
  }

  /** Change event trigger */
  public onChange(_: any) { }
  /** Dirty/touched event trigger */
  public onTouched() { }
  /** Implements ControlValueAccessor.registerOnChange */
  public registerOnChange(fn: any) { this.onChange = fn; }
  /** Implements ControlValueAccessor.registerOnTouched */
  public registerOnTouched(fn: any) { this.onTouched = fn; }

  /**
   * Initialize codemirror
   */
  private codemirrorInit(config: any) {

    if (CodeMirror) {

      this._zone.runOutsideAngular(() => {
        this._instance = CodeMirror.fromTextArea(this.host.nativeElement, config);
        this._instance.setValue(this._value);
      });


      this._instance.on('change', () => {
        this.updateValue(this._instance.getValue());
      });

      this._instance.on('focus', (instance: any, event: any) => {
        this.focus.emit({ instance, event });
      });

      this._instance.on('cursorActivity', (instance: any) => {
        this.cursorActivity.emit({ instance });
      });

      this._instance.on('blur', (instance: any, event: any) => {
        this.blur.emit({ instance, event });
      });

      this.instance$.next(this._instance);
    }
  }
}
