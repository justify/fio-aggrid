import { Component, OnInit, Input, EventEmitter } from "@angular/core";
import { IoService } from "src/app/services/io.service";
import { ToastMessage } from "./toast-message.component";
import { SPINNER_PLACEMENT, SPINNER_ANIMATIONS } from "@hardpool/ngx-spinner";
import { LoggerFactory } from 'src/app/logger-factory';
import * as _ from "lodash";

@Component({
  selector: "app-admin-ui",
  template: `
    <ngx-spinner [visible]="spin" [config]="spinnerConfig"></ngx-spinner>
    <toast-message [toast]="toast"></toast-message>
    <app-tabs></app-tabs>
  `,
  styles: []
})
export class AdminUiComponent implements OnInit {
  private logger = LoggerFactory.getLogger(this);
  
  @Input()
  allendpoint;

  @Input()
  saveendpoint;

  @Input()
  removeendpoint;

  @Input() //not in use
  userid;

  @Input()
  listrefreshinterval;

  @Input()
  set debug(on: any){
    if(!on){
      LoggerFactory.setLevel('disable')
    }else if(_.isString(on)){
      LoggerFactory.setLevel(on)
    }
  }

  toast = new EventEmitter<ToastMessage>();
  spin = false;

  spinnerConfig = {
    placement: SPINNER_PLACEMENT.block_ui,
    animation: SPINNER_ANIMATIONS.spin_1,
    size: "5rem",
    color: "#1574b3"
  };
  constructor(private io: IoService) {}

  ngOnInit() {
    this.io.setConfig({
      allEndpoint: this.allendpoint,
      saveEnpoint: this.saveendpoint,
      removeEndpoint: this.removeendpoint,
      userId: this.userid,
      listrefreshinterval: Number(this.listrefreshinterval || 3000)
    });
    this.io.toastSub$.subscribe((toast: ToastMessage) => {
      this.toast.emit(toast);
    });
    this.io.spinSub$.subscribe((show: boolean) => {
      this.spin = show;
    });
  }
}
