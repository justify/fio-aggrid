import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { IoService } from "src/app/services/io.service";
import { Observable } from "rxjs";
import { LocalGridUserPreferences } from "src/app/models/models";
import * as _ from "lodash";
import { LoggerFactory } from 'src/app/logger-factory';


@Component({
  selector: "app-table",
  template: `
    <p-table [columns]="cols" [value]="gridInstances" [paginator]="true" [rows]="15" [autoLayout]="true">
      <ng-template pTemplate="caption">
        <div style="text-align:left;padding-bottom:5px;">
          <button type="button" pButton icon="fa fa-refresh" label="Sync" (click)="refresh()"></button>
          <button type="button" pButton icon="fa fa-plus" label="Admin" (click)="addAdmin()"></button>
          <button type="button" pButton icon="fa fa-plus" label="User" (click)="addUser()"></button>
        </div>
      </ng-template>
      <ng-template pTemplate="header" let-columns>
        <tr>
          <th *ngFor="let col of columns" [pSortableColumn]="col.field">
            {{ col.header }}
            <p-sortIcon [field]="col.field"></p-sortIcon>
          </th>
        </tr>
      </ng-template>
      <ng-template pTemplate="body" let-rowData let-columns="columns">
        <tr [pSelectableRow]="rowData">
          <td *ngFor="let col of columns" style="white-space: nowrap;overflow:hidden;text-overflow:ellipsis;max-width: 150px;">
            {{ rowData[col.field] }}
          </td>
          <td>
            <div style="text-align:left;display:block;padding:2px;">
              <button type="button" pButton icon="fa fa-minus" (click)="remove(rowData)"></button>
              <button type="button" pButton icon="fa fa-copy" (click)="duplicate(rowData)"></button>
              <button type="button" pButton icon="fa fa-edit" (click)="edit(rowData)"></button>
            </div>
          </td>
        </tr>
      </ng-template>
    </p-table>
  `,
  styles: [
    `
      button {
        margin-left: 5px;
        margin-bottom: 5px;
      }
    `
  ]
})
export class TableComponent {
  private logger = LoggerFactory.getLogger(this)
  
  @Input()
  tabChanged$: Observable<number>;
  @Output()
  openEditTab = new EventEmitter<{ event: string; item: Partial<LocalGridUserPreferences> }>();

  cols = [
    { field: "role", header: "Type" },
    { field: "instanceId", header: "Id" },
    { field: "name", header: "Name" },
    { field: "userId", header: "User Id" },
    { field: "description", header: "Description" },
    { field: "appliedRequestUrl", header: "Request" },
    { field: "appliedRequestBodyJSON", header: "Request Body" }
  ];

  gridInstances: any[];
  constructor(private io: IoService) {}

  ngAfterViewInit() {
   this.refresh()
  }

  refresh(){
    this.io.getAll().subscribe((gus: LocalGridUserPreferences[]) => {
      this.gridInstances = gus;
    })
  }

  edit(rowData) {
    this.logger.trace("select: ", rowData)
    //clean out nulls
    Object.keys(rowData).map(k => {
      if (_.isString(rowData[k])) {
        const val = rowData[k];
        if (val.trim() === "null") {
          rowData[k] = "";
        }
      }
    });
    this.openEditTab.emit({ event: "edit", item: _.cloneDeep(rowData) });
  }

  addUser() {
    //testing defaults:
    // {
    //   instanceId: "0001",
    //   userId: "admin",
    //   role: "USER",
    //   description: "",
    //   mode: "",
    //   name: "pin example",
    //   appliedRequestUrl: "http://localhost:8080/ab-ag-grid-support/control/performFindList",
    //   appliedRequestVerb: "post",
    //   appliedRequestBodyJSON: "",
    //   datacreate: "",
    //   dataupdate: "",
    //   dataremove: "",
    //   gridOptionsJsString: "{columnDefs:[], custom: {}}"
    // }
    this.openEditTab.emit({
      event: "new",
      item: {
        instanceId: "",
        userId: "",
        role: "USER",
        description: "",
        appliedRequestUrl: "",
        appliedRequestVerb: "",
        appliedRequestBodyJSON: "",
        requestBody: {},
        mode: "",
        name: "",
        gridOptions: { columnDefs: [], custom: {} },
        gridOptionsJsString: "{columnDefs:[], custom: {}}"
      }
    });
  }

  addAdmin() {
    this.openEditTab.emit({
      event: "new",
      item: {
        instanceId: "",
        userId: "",
        role: "ADMIN",
        description: "",
        mode: "",
        name: "",
        appliedRequestUrl: "",
        appliedRequestVerb: "post",
        appliedRequestBodyJSON: "",
        requestBody: {},
        datacreate: "",
        dataupdate: "",
        dataremove: "",
        gridOptionsJsString: "{columnDefs:[], custom: {}}",
        gridOptions: { columnDefs: [], custom: {} }
      }
    });
  }

  duplicate(rowData) {
    this.logger.trace("duplicate: ", rowData)
    let dup = _.cloneDeep(rowData);
    dup.requestBody = {};
    dup.appliedRequestBodyJSON = null;
    this.openEditTab.emit({ event: "dup", item: dup });
  }

  remove(rowData) {
    this.logger.trace("remove: ", rowData)
    this.io.remove(rowData).subscribe(() => {
      this.io.getAll().subscribe(
        res => {
          this.logger.trace("res: ",res)
          this.gridInstances = res;
        },
        error => {
          // The Observable will throw if it's not able to recover after N attempts
          // By default it will attempts 9 times with exponential delay between each other.
          console.error(error);
        }
      );
    });
  }
}
