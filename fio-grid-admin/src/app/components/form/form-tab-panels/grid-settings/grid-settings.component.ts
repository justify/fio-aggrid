import { Component, OnInit, ViewChild, EventEmitter, Output, Input, ChangeDetectorRef } from "@angular/core";
import { ISharedCurrentLocalGridUserPreferences } from "../../form-tab.interface";
import { LocalGridUserPreferences, FioGridOptions } from "../../../../models/models";
import { CodemirrorWrapper } from '../../../ngx-codemirror/codemirror-wrapper.component';
import { IoService } from 'src/app/services/io.service';
import * as _ from 'lodash';
import { map } from 'rxjs/operators';
import { LoggerFactory } from 'src/app/logger-factory';

@Component({
  selector: "grid-settings",
  templateUrl: "./grid-settings.component.html"
})
export class GridSettingsComponent implements ISharedCurrentLocalGridUserPreferences {
  private logger = LoggerFactory.getLogger(this)

  
  myTabIndex = 0;  
  @ViewChild(CodemirrorWrapper) editor: CodemirrorWrapper;

  @Output()
  save = new EventEmitter()

  current: LocalGridUserPreferences;
  roles = [{ label: "USER", value: "USER" }, { label: "ADMIN", value: "ADMIN" }];
  verbs = [
    { label: "post", value: "post" },
    { label: "get", value: "get" },
    { label: "put", value: "put" },
    { label: "options", value: "options" }
  ];
  showEditor = true;
  genExcludeContainsStr: string;
  constructor(private io: IoService, private ref: ChangeDetectorRef) {}

  ngOnInit() {}

  setLocalGridUserPreferences(local: LocalGridUserPreferences) {
    this.current = local;
    if (!this.current.role) {
      this.current.role = "USER";
    }
    if (!this.current.appliedRequestVerb) {
      this.current.appliedRequestVerb = "post";
    }
  }

  currentTabIndex: number
  setCurrentTab(tabIndex: number){
    this.currentTabIndex = tabIndex;
    if(this.myTabIndex === tabIndex){
      this.logger.trace("grid settings: this.current : ", this.current)
    }else{
      this.ref.markForCheck()
      this.ref.detectChanges()
    }
  }

  changeFindList = false;
  onChangeFindList($event){
    // console.log("onChangeFindList: ", $event)
    if($event){
      this.current.appliedRequestUrl = "/ab-ag-grid-support/control/performFindList"
    }else{
      this.current.appliedRequestUrl = "";
    }
    // this.ref.markForCheck();
  }

  onChangeRole() {
    this.logger.trace("item (after roleOnChange): ", this.current);
  }

  codeChanged(gridOptions: any) {
    this.logger.trace("codeChanged: gridOptions: ", gridOptions);
    this.current.gridOptions = gridOptions;
  }

  autoGenerateColumnDef() {
    if (!this.current.appliedRequestUrl) {
      this.io.toastSub$.next({ type: "danger", message: "can't auto generate ColDef without a url" });
    } else {
      this.logger.trace(
        "getSampleData: ",
        this.current.appliedRequestUrl,
        this.current.appliedRequestVerb,
        this.current.appliedRequestBodyJSON,
        "is  this.item.appliedRequestBodyJSON string? ",
        _.isString(this.current.appliedRequestBodyJSON)
      );
      this.io
        .getSampleData(this.current.appliedRequestUrl, this.current.appliedRequestVerb, this.current.appliedRequestBodyJSON)
        .pipe(
          map((sampleEntity: any) => {
            this.logger.trace("sampleEntity: ", sampleEntity);
            this.current.gridOptions = this.buildColDef(sampleEntity)
          })
        )
        .subscribe();
    }
  }

  buildColDef(sampleEntity: any) {
    let excludes = [];
    if (this.genExcludeContainsStr) {
      excludes = [this.genExcludeContainsStr.trim()];
      if (this.genExcludeContainsStr.indexOf(",") > -1) {
        excludes = this.genExcludeContainsStr.split(",").map(w => w.trim().toLowerCase());
      }
    }

    const containsAny = (k: string) => {
      let contains = false;
      excludes.map(p => {
        if (k.toLowerCase().indexOf(p) > -1) {
          contains = true;
        }
      });
      return contains;
    };

    let gridOptions = {
      columnDefs: []
    };
    Object.keys(sampleEntity).map((k: string) => {
      if (!containsAny(k)) {
        gridOptions.columnDefs.push({
          headerName: _.startCase(k),
          field: k
        });
      } else {
        this.logger.trace("excluding: ", k);
      }
    });
    return gridOptions;
  }


}
