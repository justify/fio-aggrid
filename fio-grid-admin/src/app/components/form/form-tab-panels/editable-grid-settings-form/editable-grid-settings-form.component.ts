import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectorRef } from "@angular/core";
import { IoService } from 'src/app/services/io.service';
import { LocalGridUserPreferences } from 'src/app/models/models';
import { filter, map } from 'rxjs/operators';
import { ISharedCurrentLocalGridUserPreferences } from '../../form-tab.interface';
import { LoggerFactory } from 'src/app/logger-factory';

@Component({
  selector: "editable-grid-settings-form",
  templateUrl: "./editable-grid-settings-form.component.html",
  styles: [
    `
      button {
        margin-left: 5px;
      }
    `
  ]
})
export class EditableGridSettingsFormComponent implements ISharedCurrentLocalGridUserPreferences {
  private logger = LoggerFactory.getLogger(this)

  myTabIndex = 1;
  rowSelectionOptions = [{ label: "single", value: "single" }, { label: "multiple", value: "multiple" }];
  current: LocalGridUserPreferences;
  dataUniqueIdField = ""
  rowSelection = "single"
  constructor(private io: IoService, private ref: ChangeDetectorRef) {}

  setLocalGridUserPreferences(local: LocalGridUserPreferences) {
    this.current = local;
      this.logger.trace("EditableGridSettings: this.current: ", this.current)
      if(!this.current.gridOptions.custom){
        this.current.gridOptions.custom = {}
      }      
  }

  currentTabIndex: number
  setCurrentTab(tabIndex: number){
    this.currentTabIndex = tabIndex;
    if(this.myTabIndex === tabIndex){
      this.logger.trace("editable settings: this.current : ", this.current)
    }else{
      this.ref.markForCheck()
      this.ref.detectChanges()
    }
  }

  

}
