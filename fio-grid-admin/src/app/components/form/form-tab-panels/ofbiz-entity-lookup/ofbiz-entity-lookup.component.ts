import { Component, ViewChild, Input, ChangeDetectorRef } from "@angular/core";
import { LocalGridUserPreferences } from "../../../../models/models";
import { IoService } from "../../../../services/io.service";
import { Table } from "primeng/table";
import { ISharedCurrentLocalGridUserPreferences } from "../../form-tab.interface";
import * as _ from "lodash";
import { LoggerFactory } from 'src/app/logger-factory';

export interface Condition {
  fieldName: string;
  operation: string;
  value: any;
  ignoreCase: boolean;
}

@Component({
  selector: "ofbiz-entity-lookup",
  templateUrl: "./ofbiz-entity-lookup.component.html",
  styles: [
    `
      button {
        margin-left: 5px;
      }
    `
  ]
})
export class OfbizEntityLookupComponent implements ISharedCurrentLocalGridUserPreferences {
  private logger = LoggerFactory.getLogger(this)
  
  myTabIndex = 3;
  ops = [
    { label: "equals", value: "equals" },
    { label: "like", value: "like" },
    { label: "contains", value: "contains" },
    { label: "empty", value: "empty" },
    { label: "notEqual", value: "notEqual" }
  ];
  cols = [
    { field: "fieldName", header: "Field Name" },
    { field: "operation", header: "Operation" },
    { field: "value", header: "Value" },
    { field: "ignoreCase", header: "Ignore Case" }
  ];
  entityName: string = "PinCodeExample"
  newCondition: any = {
    fieldName: "",
    operation: "equals",
    value: "",
    ignoreCase: false
  };
  current: LocalGridUserPreferences;
  lookupConditions: any[];
  @ViewChild(Table) table: Table;
 
  constructor(private io: IoService, private ref: ChangeDetectorRef) {}

  setLocalGridUserPreferences(local?: LocalGridUserPreferences) {
    if(local) this.current = local;
    if (this.current.requestBody.entityName && this.current.requestBody.inputFields) {
      this.lookupConditions = !_.isEmpty(this.current.requestBody.inputFields)? this.conditionsMapASArray(this.current.requestBody.inputFields):[];
      this.entityName = this.current.requestBody.entityName
    } else{
      this.lookupConditions = [];
      this.entityName = "";
    }
  }

  currentTabIndex: number;
  setCurrentTab(tabIndex: number) {
    this.currentTabIndex = tabIndex;
    if (this.myTabIndex === tabIndex) {
      this.logger.trace("lookup: this.current : ", this.current);
      this.setLocalGridUserPreferences();
    }else{
      this.ref.markForCheck()
      this.ref.detectChanges()
      this.current.requestBody.inputFields = this.conditionsAsMap(this.lookupConditions);
      this.current.appliedRequestBodyJSON = JSON.stringify(this.current.requestBody);
    }
  }

  onEditChange(){
    this.logger.debug("after edit change: ", this.lookupConditions)
    this.current.requestBody.inputFields = this.conditionsAsMap(this.lookupConditions);
    this.current.appliedRequestBodyJSON = JSON.stringify(this.current.requestBody);
  }

  onEntityNameChange(value) {
    this.logger.trace("input change: ", arguments);
    this.current.requestBody.entityName = value;
    this.current.requestBody.inputFields = {};
    this.current.appliedRequestBodyJSON = JSON.stringify(this.current.requestBody);
  }

  fieldExists(field: string): boolean {
    return _.findIndex(this.lookupConditions, c => c.field === field) > -1;
  }

  onRowUnselect() {
    this.newCondition = {
      fieldName: "",
      operation: "equals",
      value: "",
      ignoreCase: false
    };
  }

  onRowSelect($event) {
    this.logger.trace("onRowSelect: ", $event)
    this.newCondition = $event.data;
  }

  add(rowData) {
    this.logger.trace("add: ", rowData)
    this.lookupConditions.push(_.cloneDeep(rowData));
    this.newCondition = {
      fieldName: "",
      operation: "equals",
      value: "",
      ignoreCase: false
    };
    this.current.requestBody.inputFields = this.conditionsAsMap(this.lookupConditions);
    this.current.appliedRequestBodyJSON = JSON.stringify(this.current.requestBody);
  }

  remove(rowData) {
    this.logger.trace("remove: ", rowData)
    this.lookupConditions = this.lookupConditions.filter((val: any) => !_.isEqual(rowData, val));
    this.logger.debug("after remove: ", this.lookupConditions)
    this.current.requestBody.inputFields = this.conditionsAsMap(this.lookupConditions);
    this.current.appliedRequestBodyJSON = JSON.stringify(this.current.requestBody);
  }

  conditionsAsMap(conditions: Condition[]) {
    let inputFieldEntries = [];
    conditions.forEach(condition => {
      let fieldEntry = {};
      fieldEntry[condition.fieldName + "_op"] = condition.operation;
      fieldEntry[condition.fieldName + "_ic"] = condition.ignoreCase ? "Y" : "N";
      fieldEntry[condition.fieldName + "_value"] = condition.value;
      inputFieldEntries.push(fieldEntry);
    });
    // merge maps
    let inputFieldsMap = {};
    inputFieldEntries.forEach(fieldEntry => {
      Object.keys(fieldEntry).map(k => {
        inputFieldsMap[k] = fieldEntry[k];
      });
    });
    return inputFieldsMap;
  }

  conditionsMapASArray(inputFieldsMap: any): Condition[] {
    let conditions: Condition[] = [];
    let fieldNames = _.uniq(
      Object.keys(inputFieldsMap).map(k => {
        return k.split("_")[0];
      })
    );
    this.logger.trace("fieldNames: ", fieldNames);
    fieldNames.map(fieldName => {
      conditions.push({
        fieldName: fieldName,
        operation: inputFieldsMap[fieldName + "_op"],
        value: inputFieldsMap[fieldName + "_value"],
        ignoreCase: inputFieldsMap[fieldName + "_ic"] === "Y" ? true : false
      });
    });
    this.logger.trace("conditions: ", conditions);
    return conditions;
  }
}
