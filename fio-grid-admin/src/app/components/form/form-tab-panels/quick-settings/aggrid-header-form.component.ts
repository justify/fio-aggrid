import { Component, ViewChild, Input, ChangeDetectorRef } from "@angular/core";
import * as _ from "lodash";
import { IoService } from "../../../../services/io.service";
import { ISharedCurrentLocalGridUserPreferences } from "../../form-tab.interface";
import { LocalGridUserPreferences } from "src/app/models/models";
import { Table } from "primeng/table";
import { LoggerFactory } from "src/app/logger-factory";

@Component({
  selector: "aggrid-header-form",
  templateUrl: "./aggrid-header-form.component.html",
  styles: [
    `
      button {
        margin-left: 5px;
      }
    `
  ]
})
export class AggridHeaderFormComponent implements ISharedCurrentLocalGridUserPreferences {
  private logger = LoggerFactory.getLogger(this);

  myTabIndex = 2;
  cols = [
    { field: "headerName", header: "Header Name" },
    { field: "field", header: "Data Field" },
    { field: "editable", header: "Editable" }
  ];
  cellEditors = [
    { label: "Text Cell", value: "agTextCellEditor" },
    { label: "Text Cell (Popup)", value: "agPopupTextCellEditor" },
    { label: "Large Text Cell", value: "agLargeTextCellEditor" },
    { label: "Select Cell", value: "agSelectCellEditor" },
    { label: "Select Cell (Popup)", value: "agPopupSelectCellEditor" },
    { label: "Rich Select Cell", value: "agRichSelectCellEditor" },
    { label: "Date Picker", value: "datePicker" },
    { label: "Numeric Editor", value: "numericEditor" }
  ];
  
  newHeader: any = {
    headerName: "",
    field: "",
    editable: false,
    cellEditor: null,
    _cellEditorParamsValuesStr: null,
    cellEditorParams: null
  };
  current: LocalGridUserPreferences;
  columnDefs: any[];
  @ViewChild(Table) table: Table;
  constructor(private io: IoService, private ref: ChangeDetectorRef) {}

  setLocalGridUserPreferences(local: LocalGridUserPreferences) {
    this.current = local;
    this.columnDefs = this.current.gridOptions.columnDefs;
  }

  currentTabIndex: number;
  setCurrentTab(tabIndex: number) {
    this.currentTabIndex = tabIndex;
    if (this.myTabIndex === tabIndex) {
      this.logger.trace("quick settings: this.current : ", this.current);
      this.columnDefs = this.current.gridOptions.columnDefs;
      this.logger.trace("quick settings: this.columnDefs : ", this.columnDefs);
    } else {
      this.columnDefs = null;
      this.ref.markForCheck()
      this.ref.detectChanges()
    }
  }

  showCellEditor = false;
  onCellEditorChange($event) {
    this.onChange($event);
    //onCellEditorChange:  agPopupTextCellEditor
    this.logger.debug("onCellEditorChange: ", $event);
  }

  onEditableChange($event) {
    this.showCellEditor = $event;
    if(!$event){
      this.newHeader._cellEditorParamsValuesStr = null;
      this.newHeader.cellEditor = null;
    }
    this.onChange($event);
  }

  onCellEditorParamsValuesStr($event) {
    this.newHeader._cellEditorParamsValuesStr = $event;
    this.onChange($event);
  }

  onChange($event) {
    this.logger.trace("input change: ", $event);
    this.current.gridOptions.columnDefs = this.columnDefs.map(col => {
      if (col._cellEditorParamsValuesStr) {
        col.cellEditorParams = {
          values: col._cellEditorParamsValuesStr && col._cellEditorParamsValuesStr.split(",")
        };
      }else{
        delete col.cellEditorParams;
      }
      delete col._cellEditorParamsValuesStr;
      return col;
    });
  }

  fieldExists(field: string): boolean {
    return _.findIndex(this.columnDefs, c => c.field === field) > -1;
  }

  onRowUnselect() {
    this.newHeader = {
      headerName: "",
      field: "",
      editable: false,
      cellEditor: null,
      _cellEditorParamsValuesStr: null,
      cellEditorParams: null
    };
    this.showCellEditor = false;
  }

  onRowSelect($event) {
    this.logger.info("onRowSelect: ", $event);
    this.newHeader = $event.data;
    this.showCellEditor = this.newHeader.editable;
    this.newHeader._cellEditorParamsValuesStr = this.newHeader.cellEditorParams ? this.newHeader.cellEditorParams.values.join(",") : "";
  }

  add(rowData) {
    this.logger.trace("add: ", rowData);
    this.columnDefs.push(rowData);
    this.current.gridOptions.columnDefs = this.columnDefs;
    this.clear()
  }

 clear(){
  this.newHeader = {
    headerName: "",
    field: "",
    editable: false,
    cellEditor: null,
    _cellEditorParamsValuesStr: null,
    cellEditorParams: null
  };
 }

  remove(rowData) {
    this.logger.trace("remove: ", rowData);
    this.columnDefs = this.columnDefs.filter((val: any) => !_.isEqual(rowData, val));
    this.current.gridOptions.columnDefs = this.columnDefs;
  }
}
