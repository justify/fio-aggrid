import { LocalGridUserPreferences } from 'src/app/models/models';

export interface ISharedCurrentLocalGridUserPreferences{
    setLocalGridUserPreferences(local: LocalGridUserPreferences);
    setCurrentTab(tabIndex: number);//ensures destruction of tabs so they are recreated on switch to and synced data shows
}