import { Component, OnInit, ViewChild, ChangeDetectorRef, Input } from "@angular/core";
import * as _ from "lodash";
import { IoService } from "src/app/services/io.service";
import { filter } from "rxjs/operators";
import { LocalGridUserPreferences } from "src/app/models/models";
import { EditableGridSettingsFormComponent } from './form-tab-panels/editable-grid-settings-form/editable-grid-settings-form.component';
import { GridSettingsComponent } from './form-tab-panels/grid-settings/grid-settings.component';
import { AggridHeaderFormComponent } from './form-tab-panels/quick-settings/aggrid-header-form.component';
import { ISharedCurrentLocalGridUserPreferences } from './form-tab.interface';
import { OfbizEntityLookupComponent } from './form-tab-panels/ofbiz-entity-lookup/ofbiz-entity-lookup.component';
import { LoggerFactory } from 'src/app/logger-factory';

@Component({
  selector: "app-form",
  templateUrl: "./form.component.html",
  styles: [``]
})
export class FormComponent {
  private logger = LoggerFactory.getLogger(this);
  
  @ViewChild(EditableGridSettingsFormComponent) editableSettings: ISharedCurrentLocalGridUserPreferences;
  @ViewChild(GridSettingsComponent) gridSettings: ISharedCurrentLocalGridUserPreferences;
  @ViewChild(AggridHeaderFormComponent) quickSettings: ISharedCurrentLocalGridUserPreferences;
  @ViewChild(OfbizEntityLookupComponent) lookup: ISharedCurrentLocalGridUserPreferences;

  @Input()
  set current(current: LocalGridUserPreferences){
    this.logger.trace("form: set current: ", current);
    this.local = current;
    this.userRoleDisable = (current.role !== 'ADMIN');
    this.gridSettings.setCurrentTab(0)
    this.currentChanged(current);
    this.ref.markForCheck();
  }
  local: LocalGridUserPreferences

  userRoleDisable = false;
  constructor(private io: IoService, private ref:  ChangeDetectorRef) {}

  //shared obj so mutations sync across the 3 tabs
  currentChanged($event: LocalGridUserPreferences){
    this.editableSettings.setLocalGridUserPreferences($event);
    this.gridSettings.setLocalGridUserPreferences($event);
    this.quickSettings.setLocalGridUserPreferences($event);
    this.lookup.setLocalGridUserPreferences($event);
  }

  //tabs are destroyed when switched away from, so change detection isn't an issue
  tabIndex = 0;
  handleTabChanged($event) {
    this.logger.trace("form handleTabChanged: $event.index: ", $event.index);
    this.tabIndex = $event.index;
    this.editableSettings.setCurrentTab($event.index)
    this.gridSettings.setCurrentTab($event.index)
    this.quickSettings.setCurrentTab($event.index)
    this.lookup.setCurrentTab($event.index)
  }

  
  saveCurrent(){
    this.io.save(this.local);
  }
  

  

  
}
