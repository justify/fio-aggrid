import { Component, ViewChild } from "@angular/core";
import { TabView } from "primeng/primeng";
import * as _ from "lodash";
import { IoService } from "src/app/services/io.service";
import { Subject } from "rxjs";
import { LocalGridUserPreferences } from "src/app/models/models";
import { LoggerFactory } from 'src/app/logger-factory';

export interface MainTab {
  type: "admin" | "user";
  instanceId: string;
}

@Component({
  selector: "app-tabs",
  template: `
    <div class="row">
      <div class="col-6">
        <app-table (openEditTab)="openEditTab($event)"></app-table>
      </div>
      <div *ngIf="editTabs.length > 0" class="col-6">
        <p-tabView [(activeIndex)]="index" (onClose)="handleClose($event)" (onChange)="handleTabChanged($event)">
          <p-tabPanel [closable]="true" [header]="getHeader(item)" *ngFor="let item of editTabs; let i = index" [selected]="i == 0">
            <app-form [current]="item"></app-form>
          </p-tabPanel>
        </p-tabView>
      </div>
    </div>
  `,
  styles: []
})
export class TabsComponent  {
  private logger = LoggerFactory.getLogger(this)
  
  index = 0;
  editTabs: any[] = [];

  @ViewChild(TabView) tabs: TabView;

  constructor() {}

  getHeader(item: LocalGridUserPreferences) {
    if (!item.instanceId) {
      return `new - ${item.role}`;
    } else {
      return `${item.instanceId} - ${item.role}`;
    }
  }

  tabChanged$ = new Subject<number>();
  handleTabChanged($event) {
    let activeIndex = $event.index;
    this.tabChanged$.next(activeIndex);
  }

  openEditTab(event: { event: string; item: any }) {
    this.logger.trace("event: ", event)
    switch (event.event) {
      case "edit":
        let sel = _.findIndex(this.editTabs, t => {
          return t.instanceId === event.item.instanceId && t.role === event.item.role;
        });
        this.logger.trace("sel: ", sel)
        if (sel > -1) {
          this.loadInstance(event.item, sel);
        } else {
          this.loadInstance(event.item);
        }
        break;
      case "new":
      case "dup":
        this.loadInstance(event.item);
        break;
    }

    this.logger.trace("this.editTabs.length: ", this.editTabs.length)
  }

  loadInstance(item: any, to?: number) {
    this.editTabs.push(item);
    this.setIndex(to);
  }

  setIndex(to?: number) {
    setTimeout(() => {
      let changeTo = !to ? this.editTabs.length - 1 : to;
      this.logger.trace("change tab index to: ", changeTo)
      this.index = changeTo;
    }, 300);
  }

  handleClose($event) {
    this.logger.trace("handleClose: ", $event)
    this.logger.trace("this.editTabs.length: ", this.editTabs.length)
    this.editTabs = this.editTabs.filter((val: any, indx: number) => $event.index !== indx);
    this.logger.trace("this.editTabs.length: ", this.editTabs.length)
  }
}
