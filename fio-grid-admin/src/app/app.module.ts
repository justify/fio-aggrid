import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonModule } from 'primeng/button';
import { PanelModule } from 'primeng/panel';
import { CardModule } from 'primeng/card';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { OrderListModule } from 'primeng/orderlist';
import { FieldsetModule } from 'primeng/fieldset';
import { DataViewModule } from 'primeng/dataview';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import {CheckboxModule} from 'primeng/checkbox';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ToastModule } from 'primeng/toast';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { TabViewModule } from 'primeng/tabview';
import { TableComponent } from './components/table/table.component';
import { HttpClientModule } from '@angular/common/http';
import { TabsComponent } from './components/tabs/tabs.component';
import { FormComponent } from './components/form/form.component';
import { CodemirrorComponent } from './components/ngx-codemirror/codemirror.component';
import { AdminUiComponent } from './components/admin-ui/admin-ui.component';
import { ToastMessageComponent } from './components/admin-ui/toast-message.component';
import { createCustomElement } from '@angular/elements';
import { NgxSpinnerModule } from '@hardpool/ngx-spinner';
import {EditableGridSettingsFormComponent} from './components/form/form-tab-panels/editable-grid-settings-form/editable-grid-settings-form.component'
import { CodemirrorWrapper } from './components/ngx-codemirror/codemirror-wrapper.component';
import { GridSettingsComponent } from './components/form/form-tab-panels/grid-settings/grid-settings.component';
import { AggridHeaderFormComponent } from './components/form/form-tab-panels/quick-settings/aggrid-header-form.component';
import { OfbizEntityLookupComponent } from './components/form/form-tab-panels/ofbiz-entity-lookup/ofbiz-entity-lookup.component';


const MODULES = [
  ButtonModule,
  PanelModule,
  CardModule,
  TableModule,
  ScrollPanelModule,
  FieldsetModule,
  OrderListModule,
  DataViewModule,
  DialogModule,
  DropdownModule,
  InputTextModule,
  ReactiveFormsModule,
  ToastModule,
  OverlayPanelModule,
  TabViewModule,
  CheckboxModule,
  BrowserModule,
  HttpClientModule,
  BrowserAnimationsModule,
  NgxSpinnerModule,
  FormsModule,
];





@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    TabsComponent,
    FormComponent,
    AggridHeaderFormComponent,
    CodemirrorComponent,
    AdminUiComponent,
    ToastMessageComponent,
    EditableGridSettingsFormComponent,
    CodemirrorWrapper,
    GridSettingsComponent,
    OfbizEntityLookupComponent
  ],
  imports: MODULES,
  entryComponents: [AdminUiComponent],
  providers: [],
  // bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(private injector: Injector) { }
  ngDoBootstrap() {
    // using createCustomElement from angular package it will convert angular component to stander web component
    const el = createCustomElement(AdminUiComponent, {
      injector: this.injector
    });
    // using built in the browser to create your own custom element name
    customElements.define('fio-grid-admin', el);
  }
}
