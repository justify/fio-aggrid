import * as _ from "lodash";
import * as momentImported from "moment";
const moment = momentImported;

export enum LogLevel {
  TRACE = "trace",
  DEBUG = "debug",
  INFO = "info",
  WARN = "warn",
  ERROR = "error",
  DISABLE = "disable",
  IMPORTANT = "important"
}

class LoggerFactoryStatic {
  logLevelOrder = ["trace", "debug", "info", "warn", "error", "important", "disable"];
  level = "trace";
  timestampFormat = "mm:ss:SSS";
  showObjects = true;

  setShowObjects(val: boolean) {
    this.showObjects = val;
  }

  setLevel(lev: string) {
    this.level = lev;
  }

  getLogger(loggerId: string | any) {
    let name = _.isString(loggerId) ? loggerId : loggerId.constructor.toString().match(/\w+/g)[1];
    return {
      trace: (...args) => {
        if (this.isPassLevel("trace")) {
          let msgArgs = [`%c<${moment(new Date()).format(this.timestampFormat)}>[TRACE] ${name}: `, this.handleLogArgs(args)].join("");

          return console.log(msgArgs, this.colorStyle("darkorange"));
        }
      },
      debug: (...args) => {
        if (this.isPassLevel("debug")) {
          let msgArgs = [`%c<${moment(new Date()).format(this.timestampFormat)}>[DEBUG] ${name}: `, this.handleLogArgs(args)].join("");

          return console.log(msgArgs, this.colorStyle("magenta"));
        }
      },
      info: (...args) => {
        if (this.isPassLevel("info")) {
          let msgArgs = [`%c<${moment(new Date()).format(this.timestampFormat)}>[INFO] ${name}: `, this.handleLogArgs(args)].join("");

          return console.log(msgArgs, this.colorStyle("green"));
        }
      },
      warn: (...args) => {
        if (this.isPassLevel("warn")) {
          let msgArgs = [`%c<${moment(new Date()).format(this.timestampFormat)}>[WARN] ${name}: `, this.handleLogArgs(args)].join("");

          return console.log(msgArgs, this.colorStyle("yellow"));
        }
      },
      error: (...args) => {
        if (this.isPassLevel("error")) {
          let msgArgs = [`%c<${moment(new Date()).format(this.timestampFormat)}>[ERROR] ${name}: `, this.handleLogArgs(args)].join("");

          return console.log(msgArgs, this.colorStyle("red"));
        }
      },
      important: (...args) => {
        if (this.isPassLevel("important")) {
          let msgArgs = [`%c<${moment(new Date()).format(this.timestampFormat)}>[IMPORTANT] ${name}: `, this.handleLogArgs(args)].join("");

          return console.log(msgArgs, this.colorStyle("cyan"));
        }
      }
    };
  }

  handleLogArgs(args: any[]): string {
    let logString = "";
    _.forEach(args, arg => {
      if (_.isObject(arg)) {
        logString += this.showObjects ? ` ${this.stringify(arg)}` : " [object]";
      } else {
        logString += ` ${arg}`;
      }
    });
    return logString;
  }

  isPassLevel(forLev: string): boolean {
    let indexOfFor = this.logLevelOrder.indexOf(forLev);
    let indexOfLev = this.logLevelOrder.indexOf(this.level);
    return indexOfFor >= indexOfLev;
  }

  colorStyle(color) {
    return `color: ${color};`;
  }

  stringify(obj) {
    let str = "[Circular]";
    if(this.isCyclic(obj)){
      return str
    }else{
      return JSON.stringify(obj, null, 2);
    }
  }

  private isCyclic(obj: any): boolean {
    let seenObjects = [];

    function detect(obj) {
      if (obj && typeof obj === "object") {
        if (seenObjects.indexOf(obj) !== -1) {
          return true;
        }
        seenObjects.push(obj);
        for (var key in obj) {
          if (obj.hasOwnProperty(key) && detect(obj[key])) {
            console.error("cyclic structure detail:", obj, `cycle at ${key}`);
            return true;
          }
        }
      }
      return false;
    }

    return detect(obj);
  }
}

export const LoggerFactory = new LoggerFactoryStatic();
