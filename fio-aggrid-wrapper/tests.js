var serialize = require('serialize-javascript');
var _ = require("lodash")

let target = {
    columnDefs:  [
      { headerName: 'id', field: 'id', },
      { headerName: 'Office Name', field: 'officename',  },
      { headerName: 'Pin Code', field: 'pincode',  },
      { headerName: 'Office Type', field: 'officetype',  },
      { headerName: 'Delivery Status', field: 'deliverystatus',  },
      { headerName: 'Division Name', field: 'divisionname',  },
      { headerName: 'Region Name', field: 'regionname' },
      { headerName: 'Circle Name', field: 'circlename' },
      { headerName: 'Taluk', field: 'taluk' },
      { headerName: 'District Name', field: 'districtname' },
      { headerName: 'State Name', field: 'statename' }
    ]
  }
let tstring = serialize(target);
console.log(tstring)

// {"columnDefs":[{"headerName":"id","field":"id"},{"headerName":"Office Name","field":"officename"},{"headerName":"Pin Code","field":"pincode"},{"headerName":"Office Type","field":"officetype"},{"headerName":"Delivery Status","field":"deliverystatus"},{"headerName":"Division Name","field":"divisionname"},{"headerName":"Region Name","field":"regionname"},{"headerName":"Circle Name","field":"circlename"},{"headerName":"Taluk","field":"taluk"},{"headerName":"District Name","field":"districtname"},{"headerName":"State Name","field":"statename"}]}

let target2 = {
  columnDefs:  [
    { headerName: 'id', field: 'id', },
    { headerName: 'Office Name', field: 'officename',  },
    { headerName: 'Pin Code', field: 'pincode',  },
    { headerName: 'Office Type', field: 'officetype',  },
    { headerName: 'Delivery Status', field: 'deliverystatus',  },
    { headerName: 'Division Name', field: 'divisionname',  },
    { headerName: 'Region Name', field: 'regionname' },
    { headerName: 'Circle Name', field: 'circlename' },
    { headerName: 'Taluk', field: 'taluk' },
    { headerName: 'District Name', field: 'districtname' },
    { headerName: 'State Name', field: 'statename' },
    { headerName: 'Test Update 1', field: 'id' }
  ]
}

// {"columnDefs":[{"headerName":"id","field":"id"},{"headerName":"Office Name","field":"officename"},{"headerName":"Pin Code","field":"pincode"},{"headerName":"Office Type","field":"officetype"},{"headerName":"Delivery Status","field":"deliverystatus"},{"headerName":"Division Name","field":"divisionname"},{"headerName":"Region Name","field":"regionname"},{"headerName":"Circle Name","field":"circlename"},{"headerName":"Taluk","field":"taluk"},{"headerName":"District Name","field":"districtname"},{"headerName":"State Name","field":"statename"},{"headerName":"Test Update 1","field":"id"}]}

let tstring2 = serialize(target2);
console.log(tstring2)

// function deserialize(serializedJavascript){
//     return eval('(' + serializedJavascript + ')');
// }

// let deserialized = deserialize(tstring);
// deserialized.testIsObject = {test: "hello"}

console.log(JSON.stringify(deserialized, null, 2))