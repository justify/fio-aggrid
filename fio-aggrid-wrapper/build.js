const fs = require("fs-extra");
const concat = require("concat");
const Terser = require("terser");
const path = require("path");

(async function build() {
  const files = [
    "./dist/fio-ag-grid/runtime.js",
    "./dist/fio-ag-grid/polyfills.js",
    "./dist/fio-ag-grid/scripts.js",
    "./dist/fio-ag-grid/vendor.js",
    "./dist/fio-ag-grid/main.js"
  ];

  await fs.ensureDir("src/web-component-build");
  concat(files).then(source => {
    const dstPath = `src/web-component-build`
    const buildFile = `fio-ag-grid-${fs.readJsonSync("./package.json").version}.js`
    const ofbizDstPath = `/home/justin/Documents/fio/ofbiz-release15.12/hot-deploy/ab-ag-grid-support/webapp`
    fs.writeFileSync(path.join(dstPath, buildFile), Terser.minify(source).code, "utf8");
    fs.copyFileSync(path.join(dstPath, buildFile), path.join(ofbizDstPath, 'assets', buildFile))
    console.log("copied wc to ofbiz dest")
    fs.copyFileSync(`./src/web-component-build/agGridMacro.ftl`, path.join(ofbizDstPath, `agGridMacro.ftl`))
    console.log("copied ftl to ofbiz dest")
  });
})();
