import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  template: `
    <fio-ag-ab-grid
      [autosizeallcol]="true"
      [debug]="true"
      [userid]="'admin'"
      [instanceid]="'standalone'"
      [preferenceendpointfetch]="configendpointfetch"
      [preferenceendpointsave]="configendpointsave"
      [datacreate]="datacreate"
      [dataupdate]="dataupdate"
      [dataremove]="dataremove"
      [gridoptions]="gridoptions"
      [aggridthemeclass]="aggridthemeclass"
      [endpoint]="endpoint"
      [requestbody]="requestbody"
      [shownotifications]="true"
    ></fio-ag-ab-grid>
  `
})
export class AppComponent {
  endpoint = "https://localhost:8443/ab-ag-grid-support/control/performFindList";
  configendpointfetch = "https://localhost:8443/ab-ag-grid-support/control/getGridUserConfig";
  configendpointsave = "https://localhost:8443/ab-ag-grid-support/control/saveGridUserConfig";
  datacreate = "https://localhost:8443/ab-ag-grid-support/control/dataCreate?entity-auto=createPinCodeExample2";
  dataupdate = "https://localhost:8443/ab-ag-grid-support/control/dataUpdate?entity-auto=updatePinCodeExample2";
  dataremove = "https://localhost:8443/ab-ag-grid-support/control/dataRemove?entity-auto=deletePinCodeExample2";
  // datacreate = "none";
  // dataupdate = "none";
  // dataremove = "none";


  requestbody = {
    "entityName": "PinCodeExample2",
    "inputFields": '{}',
    "noConditionFind": "Y",
    "startRow":0,
    "endRow":20
  }

  gridoptions = {
    custom: {
      dataUniqueIdField: "id",
      rowSelection: "multiple"
    }
  };
  aggridthemeclass = "ag-theme-balham-dark";
  aggridstylestr = "width: 100%; height: 100%;";
}
