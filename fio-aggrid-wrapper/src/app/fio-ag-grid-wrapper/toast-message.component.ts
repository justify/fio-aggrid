import { Component, OnInit, Input, EventEmitter } from "@angular/core";
import * as _ from "lodash";
import { filter } from 'rxjs/operators';

export class ToastMessage {
  type: string;
  message: string;
}

@Component({
  selector: "toast-message",
  styles: [
    `
      div.floating-alert {
        position: fixed;
        right: 1%;
        z-index: 10;
        font-size: 2rem;
      }
    `
  ],
  template: `
    <div *ngFor="let aToast of toasts; let i = index">
      <div
        [ngStyle]="notificationstyle"
        class="floating-alert alert alert-{{
          aToast.type
        }} alert-dismissible fade show"
        [style.top]="(i + 1) * 55 + 'px'"
        role="alert"
      >
        <strong>{{ aToast.message }}</strong>
        <button
          type="button"
          class="close"
          data-dismiss="alert"
          aria-label="Close"
        >
          <span aria-hidden="true" (click)="closeAlert(i)">&times;</span>
        </button>
      </div>
    </div>
  `
})
export class ToastMessageComponent {
  @Input()
  toast: EventEmitter<ToastMessage>;
  toasts: ToastMessage[] = [];

  ngAfterViewInit(): void {
    if (this.toast) {
      this.toast.pipe(filter(t => t.type === "danger" ||  t.type === "success")).subscribe((t: ToastMessage) => {
        this.toasts.push(t);
        const toRemove = this.toasts.length - 1;
        setTimeout(() => {
          this.closeAlert(toRemove);
        }, 10000);
      });
    }
  }

  closeAlert(i: number) {
    if (i === 0) {
      this.toasts = [];
    } else {
      this.toasts = this.toasts.splice(i, 1);
    }
  }
}
