import { GridOptions } from "ag-grid-community";

export interface FioGridOptions extends GridOptions {
  custom?: CustomGridOptions;
}

export interface CustomGridOptions {
  filterModel?: any;
  dataUniqueIdField?: string;
  rowSelection?: "single" | "multiple";
  csvExportOptions?: CsvExportOptions;
}

export interface CsvExportOptions {
  skipHeader?: boolean;
  columnGroups?: boolean;
  skipFooters?: boolean;
  skipGroups?: boolean;
  skipPinnedTop?: boolean;
  skipPinnedBottom?: boolean;
  allColumns?: boolean;
  onlySelected?: boolean;
  fileName?: string;
  sheetName?: string;
  exportMode?: "csv";
}

export interface ButtonBarButton {
  label: string;
  clickEventId: string;
  styleClass: string;
}

export interface ButtonClickEvent {
  instanceid: string;
  clickEventId: string;
}

export interface EditedCell {
  originalValue: any;
  newValue: any;
  uniqueId: number | string;
  field: string;
}

export interface OriginalOfEditedCell {
  value: any; 
  uniqueId: number | string;
  field: string;
}
