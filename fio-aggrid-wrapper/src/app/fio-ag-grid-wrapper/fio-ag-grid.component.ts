import {
  Component,
  ViewChild,
  Input,
  EventEmitter,
  NgZone,
  Output
} from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { tap, map, filter, flatMap, take, catchError } from "rxjs/operators";
import { Observable, of, concat, forkJoin } from "rxjs";
import * as _ from "lodash";
import * as sjs from "serialize-javascript";
import {
  GridApi,
  ColumnApi,
  GridOptions,
  ColDef,
  Column,
  FilterManager
} from "ag-grid-community";
import { ToastMessage } from "./toast-message.component";
import { SPINNER_PLACEMENT, SPINNER_ANIMATIONS } from "@hardpool/ngx-spinner";
import { DatePickerComponent } from "./framework-components/date-picker/date-picker.component";
import { NumericEditorComponent } from "./framework-components/numeric-editor/numeric-editor.component";
import { AgGridNg2 } from "ag-grid-angular";
import {
  ButtonBarButton,
  ButtonClickEvent,
  FioGridOptions,
  CustomGridOptions,
  CsvExportOptions,
  EditedCell,
  OriginalOfEditedCell
} from "./models";

@Component({
  selector: "fio-ag-ab-grid",
  template: `
    <div id="fag-grid-wrapper">
      <toast-message
        *ngIf="_shownotifications"
        [toast]="notifications"
      ></toast-message>
      <ngx-spinner
        *ngIf="_showuserprefspinner"
        [visible]="spinner"
        [config]="spinnerConfig"
      ></ngx-spinner>
      <div *ngIf="!_hidebuttonbar" class="button-bar">
        <button
          id="refreshUserPreferencesBtn"
          type="button"
          class="btn btn-primary"
          (click)="buttonBarClick('builtin_refreshUserPreferences')"
        >
          refresh preferences
        </button>
        <button
          id="saveUserPreferencesBtn"
          type="button"
          class="btn btn-primary"
          (click)="buttonBarClick('builtin_saveUserPreferences')"
        >
          save preferences
        </button>
        <button
          id="saveUpdatesBtn"
          type="button"
          class="btn btn-primary"
          *ngIf="editMode && !!_dataupdate"
          (click)="buttonBarClick('builtin_saveUpdates')"
        >
          save updates
        </button>
        <button
          id="removeSelectedBtn"
          type="button"
          class="btn btn-primary"
          *ngIf="editMode && !!_dataremove"
          (click)="buttonBarClick('builtin_removeSelected')"
        >
          remove selected
        </button>
        <button
          id="insertNewRowBtn"
          type="button"
          class="btn btn-primary"
          *ngIf="editMode && !!_datacreate"
          (click)="buttonBarClick('builtin_insertNewRow')"
        >
          insert new
        </button>
        <button
          id="excelExportOptionsBtn"
          type="button"
          class="btn btn-primary"
          (click)="buttonBarClick('builtin_csvExport')"
        >
          excel export
        </button>
        <button
          type="button"
          *ngFor="let button of _buttonbarbuttons"
          class="{{ button.styleClass }}"
          (click)="buttonBarClick(button.clickEventId)"
        >
          {{ button.label }}
        </button>
      </div>
      <ag-grid-angular
        *ngIf="userPrefLoaded"
        #agGrid
        id="{{ instanceid }}"
        class="{{ aggridthemeclass }}"
        [defaultColDef]="defaultColDef"
        [frameworkComponents]="frameworkComponents"
        [gridOptions]="mergedGridOptions"
        [debug]="false"
        [rowSelection]="
          (mergedGridOptions.custom && mergedGridOptions.custom.rowSelection) ||
          'single'
        "
        (gridReady)="onGridReady($event)"
        [stopEditingWhenGridLosesFocus]="editMode"
        (firstDataRendered)="onFirstDataRendered($event)"
        (columnMoved)="userGridChanged($event)"
        (gridColumnsChanged)="userGridChanged($event)"
        (columnRowGroupChanged)="userGridChanged($event)"
        (newColumnsLoaded)="userGridChanged($event)"
        (columnVisible)="userGridChanged($event)"
        (columnPinned)="userGridChanged($event)"
        (columnGroupOpened)="userGridChanged($event)"
        (columnResized)="userGridChanged($event)"
        (filterChanged)="userGridChanged($event)"
        (sortChanged)="userGridChanged($event)"
        (cellValueChanged)="onCellValueChanged($event)"
        (selectionChanged)="onSelectionChanged($event)"
      ></ag-grid-angular>
    </div>
  `
})
export class FioAgGridComponent {
  private logger = {
    debug: (...args) => {
      if (this._debug) {
        console.log(args);
      }
    }
  };
  private gridApi: GridApi;
  private gridColumnApi: ColumnApi;

  @ViewChild("agGrid") grid: AgGridNg2;

  spinnerConfig = {
    placement: SPINNER_PLACEMENT.block_ui,
    animation: SPINNER_ANIMATIONS.spin_1,
    size: "5rem",
    color: "#1574b3"
  };
  defaultServerSideGridOptions = {
    animateRows: false,
    cacheBlockSize: 10000,
    infiniteInitialRowCount: 9000,
    maxBlocksInCache: 100,
    rowDeselection: true,
    maxConcurrentDatasourceRequests: 2,
    blockLoadDebounceMillis: 10
  };
  defaultClientSideGridOptions = {};
  defaultColDef = {
    resizable: true,
    sortable: true,
    filter: true
  };
  rowData: any[] = [];
  wrapperOptions: any;
  userPrefLoaded = false;
  firstLoad = true;
  currentUserGridOptions: FioGridOptions = {};
  currentAdminGridOptions: FioGridOptions = {};
  mergedGridOptions: FioGridOptions;
  filterManager: FilterManager;

  editMode: boolean;

  @Output()
  buttonBarClickEvent = new EventEmitter<ButtonClickEvent>();
  @Output()
  notifications = new EventEmitter<ToastMessage>();
  @Output()
  spinnerEvents = new EventEmitter<boolean>();

  @Input()
  preferenceendpointfetch: string;
  @Input()
  preferenceendpointsave: string;
  @Input()
  instanceid: string;
  @Input()
  userid: string;
  @Input()
  aggridthemeclass: string;
  @Input()
  aggridstylestr: string;

  _gridtitle = "";

  @Input()
  set gridtitle(val: string) {
    if (val !== "none") {
      this._gridtitle = val;
    }
  }

  _endpointverb = "post";
  @Input()
  set endpointverb(val: string) {
    if (val !== "none") {
      this._endpointverb = val;
    }
  }

  _endpoint: string;
  @Input()
  set endpoint(val: string) {
    if (val !== "none") {
      this._endpoint = val;
    }
  }

  _debug: boolean;
  @Input()
  set debug(on: any) {
    this._debug = _.isString(on) ? on === "true" : on;
    if (on === "false") {
    }
  }

  _hidebuttonbar: boolean = false;
  @Input()
  set hidebuttonbar(val: any) {
    this._hidebuttonbar = _.isString(val) ? val === "true" : val;
  }

  _showuserprefspinner: boolean = false;
  @Input()
  set showuserprefspinner(val: any) {
    this._showuserprefspinner = _.isString(val) ? val === "true" : val;
  }
  _shownotifications: boolean = false;
  @Input()
  set shownotifications(val: any) {
    this._shownotifications = _.isString(val) ? val === "true" : val;
  }
  _autosizeallcol: boolean;
  @Input()
  set autosizeallcol(val: any) {
    this._autosizeallcol = _.isString(val) ? val === "true" : val;
  }

  @Input()
  set initialdata(data: any) {
    this.setRowData(_.isString(data) ? JSON.parse(data) : data);
  }

  _gridoptions: any;
  @Input()
  set gridoptions(val: any) {
    if (val !== "none") {
      this._gridoptions = _.isString(val) ? JSON.parse(val) : val;
    }
  }

  _buttonbarbuttons: ButtonBarButton[];
  @Input()
  set buttonbarbuttons(val: any) {
    if (val !== "none") {
      this._buttonbarbuttons = _.isString(val) ? JSON.parse(val) : val;
    }
  }

  _requestbody: any;
  @Input()
  set requestbody(val: any) {
    if (val !== "none") {
      this._requestbody = _.isString(val) ? JSON.parse(val) : val;
    }
  }

  @Input()
  set datacreate(val: string) {
    this._datacreate = val !== "none" ? val : null;
  }
  _datacreate: string;
  @Input()
  set dataupdate(val: string) {
    this.logger.debug("set dataupdate: ", val);
    this._dataupdate = val !== "none" ? val : null;
  }
  _dataupdate: string;
  @Input()
  set dataremove(val: string) {
    this._dataremove = val !== "none" ? val : null;
  }
  _dataremove: string;

  _notificationstyle: any;
  @Input()
  set notificationstyle(val: any) {
    if (val !== "none") {
      this._notificationstyle = _.isString(val) ? JSON.parse(val) : val;
    }
  }

  frameworkComponents = {
    datePicker: DatePickerComponent,
    numericEditor: NumericEditorComponent
  };
  constructor(private http: HttpClient, public zone: NgZone) {
    /**
     * Used to decide when addEventListener can be attached to the
     * grid instance element, within the callback this is the element
     */
    (<any>window).fagReady = (instanceid, callback) => {
      var _this = this
      return (instanceid, callback) => {
        var element = document.getElementById(instanceid);
        var check = setInterval(() => {
          if (
            element &&
            _this.gridApi &&
            _this.gridColumnApi &&
            _this.isAfterViewInit
          ) {
            clearInterval(check);
            callback(element,  _this.gridApi,  _this.gridColumnApi,  _this);
          }
        }, 50);
      };
    };
  }

  /**
   * For Examples
   */
  testNotification(type: string) {
    this.notifications.emit({
      type: type,
      message: "This is a test notification"
    });
  }

  logGridWrapperInfo() {
    this.logger.debug("grid instance info: ", {
      instanceid: this.instanceid,
      customGridOptions: this.mergedGridOptions.custom,
      rowDataLength: this.rowData.length,
      editMode: this.editMode
    });
  }

  isAfterViewInit = false;
  ngAfterViewInit(): void {
    this.isAfterViewInit = true;
  }

  buttonBarClick(clickEventId: string) {
    switch (clickEventId) {
      case "builtin_saveUserPreferences":
        this.saveUserPreferences();
        break;
      case "builtin_refreshUserPreferences":
        this.refreshUserPreferences();
        break;
      case "builtin_saveUpdates":
        this.saveUpdates();
        break;
      case "builtin_removeSelected":
        this.removeSelected();
        break;
      case "builtin_insertNewRow":
        this.insertNewRow();
        break;
      case "builtin_csvExport":
        this.csvExport();
        break;
    }
    this.buttonBarClickEvent.emit({
      instanceid: this.instanceid,
      clickEventId
    });
  }

  showAllColumns() {
    this.gridColumnApi.getAllColumns().map((col: Column) => {
      this.gridColumnApi.setColumnVisible(col, true);
    });
  }

  clearAllColumnFilters() {
    this.gridColumnApi.getAllColumns().map((col: Column) => {
      this.filterManager.destroyFilter(col);
    });
    delete this.currentUserGridOptions.custom.filterModel;
  }

  getRowData() {
    return this.rowData;
  }
  setRowData(rows: any[]) {
    this.rowData = rows;
    if (this.gridApi) {
      this.gridApi.setRowData(this.rowData);
      this.applyCustomSavedFilters();
    }
  }

  csvExport() {
    let params;
    if (this.mergedGridOptions.custom.csvExportOptions) {
      params = _.defaultsDeep(this.mergedGridOptions.custom.csvExportOptions, {
        exportMode: "csv",
        fileName: `${this.instanceid}.csv`
      });
    } else {
      params = {
        exportMode: "csv",
        fileName: `${this.instanceid}.csv`
      };
    }
    //default behaviors is if rows selected export only selected, unless config
    if (this.isRowSelected) {
      params.onlySelected = true;
    } else {
      params.onlySelected = false;
    }
    this.gridApi.exportDataAsCsv(params);
  }

  spinner = false;
  spin() {
    this.spinner = !this.spinner;
    this.spinnerEvents.emit(this.spinner);
  }

  originalOfCellsChanged: OriginalOfEditedCell[] = [];
  cellsChanged = new Map<string, EditedCell>();
  onCellValueChanged(changeEvent: any) {
    this.logger.debug("onCellValueChanged: ", changeEvent);
    const key = `${changeEvent.colDef.colId}:${changeEvent.rowIndex}`;
    if (this.cellsChanged.has(key)) {
      const originalValue = this.cellsChanged.get(key).originalValue;
      this.cellsChanged.set(key, {
        originalValue: originalValue,
        newValue: changeEvent.newValue,
        uniqueId:
          changeEvent.data[this.mergedGridOptions.custom.dataUniqueIdField],
        field: changeEvent.colDef.field
      });
    } else {
      this.cellsChanged.set(key, {
        originalValue: changeEvent.oldValue,
        newValue: changeEvent.newValue,
        uniqueId:
          changeEvent.data[this.mergedGridOptions.custom.dataUniqueIdField],
        field: changeEvent.colDef.field
      });
      this.originalOfCellsChanged.push({
        uniqueId:
          changeEvent.data[this.mergedGridOptions.custom.dataUniqueIdField],
        field: changeEvent.colDef.field,
        value: changeEvent.oldValue
      });
    }
  }

  private revertFailedUpdatesFromResponse(response: any) {
    console.log("revertFailedUpdatesFromResponse");
    if (response.failedEntities && response.failedEntities.length > 0) {
      const failedIds = response.failedEntities.map(
        d => d[this.mergedGridOptions.custom.dataUniqueIdField]
      );
      this.logger.debug("revert failed updates in grid data");
      _.values(_.groupBy(this.originalOfCellsChanged, "uniqueId")).map(
        (cells: OriginalOfEditedCell[]) => {
          const id = _.first(cells).uniqueId;
          if (failedIds.includes(id)) {
            let oldRow = {};
            oldRow[this.mergedGridOptions.custom.dataUniqueIdField] = id;
            cells.map((cell: OriginalOfEditedCell) => {
              oldRow[cell.field] = cell.value;
            });
            this.logger.debug(
              "reverting failed update op to old data: ",
              oldRow
            );
            this.updateRowInGridFromResponse(oldRow);
          }
        }
      );
    }
  }

  clearChangeRecord() {
    this.logger.debug(
      "clearing cellsChanged & originalOfCellsChanged after response"
    );
    this.cellsChanged = new Map<string, EditedCell>();
    this.originalOfCellsChanged = [];
    this.gridApi.deselectAll();
  }

  //issue with sorting, if you sort the row indexes will change ... using this.mergedGridOptions.dataUniqueIdField
  saveUpdates() {
    let obs$ = [];
    const changedCells = Array.from(this.cellsChanged.values()).filter(
      c => c.originalValue !== c.newValue
    );
    const rowGroupedChanges = _.groupBy(changedCells, "uniqueId");
    // this.logger.debug("save: ", rowGroupedChanges);
    let changedRecords = [];
    _.values(rowGroupedChanges).map((recordEditedCells: EditedCell[]) => {
      let editedRecord = {};
      editedRecord[this.mergedGridOptions.custom.dataUniqueIdField] = _.first(
        recordEditedCells
      ).uniqueId;
      recordEditedCells.map((editedCell: EditedCell) => {
        editedRecord[editedCell.field] = editedCell.newValue;
      });
      changedRecords.push(editedRecord);
    });
    // this.logger.debug("push delta to endpoint: ", changedRecords);
    let updateChangedRecord = _.filter(changedRecords, (r: any) => {
      return !r[this.mergedGridOptions.custom.dataUniqueIdField].startsWith(
        "__id-"
      );
    });
    this.logger.debug("updateChangedRecord: ", updateChangedRecord);
    if (updateChangedRecord.length > 0) {
      obs$.push(
        this.crudCall(this._dataupdate, updateChangedRecord).pipe(
          map((response: any) => {
            //filter out successfully updated
            this.revertFailedUpdatesFromResponse(response);
          })
        )
      );
    }
    let createChangedRecord = _.filter(changedRecords, (r: any) => {
      if (
        r[this.mergedGridOptions.custom.dataUniqueIdField].startsWith("__id-")
      ) {
        r.tempId = r[this.mergedGridOptions.custom.dataUniqueIdField];
        r[this.mergedGridOptions.custom.dataUniqueIdField] = null;
        return true;
      } else {
        return false;
      }
    });
    this.logger.debug("createChangedRecord: ", createChangedRecord);
    if (createChangedRecord.length > 0) {
      obs$.push(
        this.crudCall(this._datacreate, createChangedRecord).pipe(
          map((response: any) => {
            this.logger.debug("datacreate: response: ", response);
            this.newTempId = 0;

            response.entities.map((row: any) => {
              this.updateRowInGridFromResponse(row);
            });
          })
        )
      );
    }
    console.log("obs$: ", obs$);
    forkJoin(obs$)
      .pipe(
        map(() => {
          this.clearChangeRecord();
        })
      )
      .subscribe();
  }

  private updateRowInGridFromResponse(row: any) {
    row = _.omit(row, ["userLogin"]);
    const rowNode = this.gridApi.getRowNode(
      row.tempId || row[this.mergedGridOptions.custom.dataUniqueIdField]
    );
    Object.keys(row).map(k => {
      if (k !== "tempId") {
        this.logger.debug("row: key: ", k, ", value: ", row[k]);
        rowNode.setDataValue(k, row[k]);
      }
    });
  }

  selectedIndexPosition = 0;
  selectedRowData: any;
  isRowSelected = false;
  onSelectionChanged($event) {
    const selectedNode = _.first(this.gridApi.getSelectedNodes());
    if (!selectedNode) {
      this.isRowSelected = false;
      return;
    }
    // this.logger.debug("onSelectionChanged: ", selectedNode);
    this.selectedIndexPosition = selectedNode.rowIndex;
    this.selectedRowData = selectedNode.data;
    // this.logger.debug("selectedIndexPosition: ", this.selectedIndexPosition);
    // this.logger.debug("selectedRowData: ", this.selectedRowData);
    this.isRowSelected = true;
  }

  removeSelected() {
    if (!this.isRowSelected) {
      this.notifications.emit({
        type: "danger",
        message:
          "This action cannot be performed without a row first being selected!"
      });
      return;
    }
    const selectedRows = this.gridApi.getSelectedRows();
    // this.logger.debug("removeSelected: ", selectedRows);
    //add backend call
    //res {successes: 3, entities: Array(3)}
    this.crudCall(this._dataremove, selectedRows).subscribe((res: any) => {
      //so now need to remove only thoes that where successfully deleted
      let successFullyRemoved = selectedRows
        .map(r => {
          if (
            _.findIndex(
              res.entities,
              e =>
                e[this.mergedGridOptions.custom.dataUniqueIdField] ===
                r[this.mergedGridOptions.custom.dataUniqueIdField]
            ) > -1
          ) {
            return r;
          }
        })
        .filter(r => !!r);
      const rmvRes = this.gridApi.updateRowData({
        remove: successFullyRemoved
      });
      this.logger.debug("removeSelected: updateRowData:rmvRes: ", rmvRes);
    });
  }

  insertNewRow() {
    if (!this.isRowSelected) {
      this.notifications.emit({
        type: "danger",
        message:
          "This action cannot be performed without a row first being selected!"
      });
      return;
    }

    const newItem = this.createNewRowData();
    const res = this.gridApi.updateRowData({
      add: [newItem],
      addIndex: this.selectedIndexPosition
    });
  }

  /**
   * take the selected row and clean out the values
   */
  newTempId = 0;
  createNewRowData(): any {
    let empyRow: any = {};
    Object.keys(this.selectedRowData).map(k => (empyRow[k] = null));
    //info encoded into an id never a good idea, but in this case means the update delta logic works, as is
    //the action to save updates via the button, can now build it's delta using newTempId for grouping the edited
    //fields. It can then decide which set to end to create endpoint vs new endpoint.
    //The logic sends to the create endpoint after deleting the newTempId, this will not work where a primary key is editable in the grid
    //after the request newTempId is set back to 0;
    empyRow[this.mergedGridOptions.custom.dataUniqueIdField] = `__id-${this
      .newTempId++}`;
    return empyRow;
  }

  //have to set content type to custom to prevent ofbiz from trying to pass the request body, since it expects a map and does some weird
  //stuff with it you can't send a body that's not a map, and even if you wrap the content in a map, you can't read it cause it's already been
  //read by ofbiz internals
  crudCall(endpoint: string, body: any): Observable<any> {
    this.logger.debug("crudCall: ", endpoint, ", body: ", body);
    let isOfbiz = false;
    if (endpoint.indexOf("entity-auto=") > -1) {
      isOfbiz = true;
    }
    return this.http
      .post(endpoint, body, {
        headers: new HttpHeaders()
          .set(
            "Content-Type",
            isOfbiz ? "ofbiz-bypass/json" : "application/json"
          )
          .set("Response-Type", "application/json")
      })
      .pipe(
        map((res: any) => {
          this.logger.debug("update res: ", res);
          res.errors &&
            res.errors.map(err => {
              this.notifications.emit({
                type: "danger",
                message: err.errorMessage
              });
            });
          if (res.successes > 0) {
            this.notifications.emit({
              type: "success",
              message: `${res.successes} entity operations completed successfully!`
            });
          }
          this.notifications.emit({
            type: "crud-reponse",
            message: res
          });
          return res;
        })
      );
  }

  refreshUserPreferences() {
    this.logger.debug("calling receiveGridUserPreferences");
    this.spin();
    this.receiveGridUserPreferences(this.wrapperOptions, true).subscribe(() =>
      this.spin()
    );
  }

  saveUserPreferences() {
    this.logger.debug("isCyclic: ", this.isCyclic(this.currentUserGridOptions));
    this.logger.debug("save: ", this.currentUserGridOptions);
    let req = {
      userGridOptions: sjs(this.currentUserGridOptions),
      userid: this.userid,
      instanceid: this.instanceid
    };
    this.logger.debug(
      "#3 POST gridOptionsChanged: ",
      this.currentUserGridOptions
    );
    return this.http
      .post(`${this.preferenceendpointsave}`, req, {
        headers: new HttpHeaders().set("Content-Type", "ofbiz-bypass/json")
      })
      .pipe(
        catchError((err: any) => {
          let msg: string = err.message.substring(
            err.message.lastIndexOf(":"),
            err.message.length
          );
          this.logger.debug("err msg: ", msg);
          this.notifications.emit({ type: "danger", message: msg });
          return of(false);
        }),
        take(1)
      )
      .subscribe(success => {
        if (success !== false) {
          this.notifications.emit({ type: "success", message: "saved!" });
        }
      });
  }

  ngOnInit() {
    this.wrapperOptions = {
      preferenceendpointfetch: this.preferenceendpointfetch,
      userid: this.userid,
      instanceid: this.instanceid,
      lastUpdateTime: -1
    };
    //  this.logger.debug("wrapperOptions: ", this.wrapperOptions);
    this.logger.debug("ngOnInit calling receiveGridUserPreferences");
    this.receiveGridUserPreferences(this.wrapperOptions)
      .pipe(take(1))
      .subscribe();
  }

  detectColDefHasEditable(columnDefs: any[]): boolean {
    let editable = false;
    columnDefs.map(col => {
      Object.keys(col).map(k => {
        if (k === "editable" && col[k] === true) {
          editable = true;
        }
      });
    });
    return editable;
  }

  isAdminChanged(remoteGridOptions: any) {
    const adminChanged = !_.isEqual(
      _.pick(remoteGridOptions, ["columnDefs"]),
      _.pick(this.currentAdminGridOptions, ["columnDefs"])
    );
    // if (this._debug && adminChanged) {
    //   this.logger.debug("local ADMIN: ", this.currentAdminGridOptions);
    //   this.logger.debug("remote ADMIN: ", remoteGridOptions);
    // }
    return adminChanged;
  }

  isUserChanged(remoteGridOptions: any) {
    const userChanged = !_.isEqual(
      _.pick(remoteGridOptions, ["columnDefs"]),
      _.pick(this.currentUserGridOptions, ["columnDefs"])
    );
    // if (this._debug && userChanged) {
    //   this.logger.debug("local USER: ", this.currentUserGridOptions);
    //   this.logger.debug("remote USER: ", remoteGridOptions);
    // }
    return userChanged;
  }

  //doesn't seem to work
  // private difference(object, base) {
  //   const changes = (object, base) => {
  //     return _.transform(object, (result, value, key) => {
  //       if (!_.isEqual(value, base[key])) {
  //         result[key] = (_.isObject(value) && _.isObject(base[key])) ? changes(value, base[key]) : value;
  //       }
  //     });
  //   }
  //   return changes(object, base);
  // }

  gridChangedCount = 0;
  userGridChanged($event) {
    if (this.gridColumnApi) {
      {
        //cyclic structures
        // this.logger.debug("ui change event: ", $event);
        // this.logger.debug("this.gridColumnApi.getAllDisplayedColumns(): ", this.gridColumnApi.getAllDisplayedColumns())
      }
      let updated: any;
      switch ($event.type) {
        case "sortChanged":
          updated = this.mergeColumnStateToCurrentUserGridOptionsCopy(
            this.gridColumnApi
              .getAllDisplayedColumns()
              .map((column: Column) => {
                let update = {
                  colId: column.getColId(),
                  sort: column.getSort() === undefined ? null : column.getSort() //there are 3 states to sort asc desc and undefined, the merge op ignores undefined do use null instead
                };
                this.logger.debug("sortChanged: update: ", update);
                return update;
              })
          );
          break;
        case "columnResized":
          if ($event.finished && this.gridChangedCount > 0) {
            //ignore first change
            updated = this.mergeColumnStateToCurrentUserGridOptionsCopy(
              this.gridColumnApi.getColumnState()
            );
          }
          break;
        case "columnMoved":
          let withAddedOrderIndex = this.gridColumnApi
            .getColumnState()
            .map((val: any, index: number) => {
              val.orderedIndex = index;
              return val;
            });
          updated = this.mergeColumnStateToCurrentUserGridOptionsCopy(
            withAddedOrderIndex
          );
          break;
        case "filterChanged":
          this.filterManager = $event.api.filterManager;
          let filterModel = this.filterManager.getFilterModel();
          // let updated1 = this.mergeColumnStateToCurrentUserGridOptionsCopy(
          //   this.gridColumnApi.getColumnState()
          // );
          updated = this.saveColumnFilterModels(filterModel);
          // if(_.isEmpty(updated2.custom.filterModel)){
          //   delete updated1.custom.filterModel
          // }
          // updated = _.defaultsDeep(updated2, updated1);
          break;
        default:
          updated = this.mergeColumnStateToCurrentUserGridOptionsCopy(
            this.gridColumnApi.getColumnState()
          );
      }
      this.currentUserGridOptions = updated;
    }

    this.gridChangedCount++;
  }

  saveColumnFilterModels(filterModel: any): FioGridOptions {
    this.logger.debug("filterModel: ", filterModel);
    let updatedGridOptions = _.cloneDeep(this.currentUserGridOptions);
    this.logger.debug("currentUserGridOptions: ", updatedGridOptions);
    let updated = {
      columnDefs: updatedGridOptions.columnDefs,
      custom: {
        filterModel: _.cloneDeep(filterModel)
      }
    };
    this.logger.debug("updated grid options: ", updated);
    return updated;
  }

  mergeColumnStateToCurrentUserGridOptionsCopy(
    colStates: any[]
  ): FioGridOptions {
    this.logger.debug("colState: ", colStates);
    let updatedGridOptions = _.cloneDeep(this.currentUserGridOptions);
    let updatedColDefs = updatedGridOptions.columnDefs.map((colDef: ColDef) => {
      let targetColSate = _.find(
        colStates,
        (c: any) => c.colId === colDef.colId
      );
      return _.defaultsDeep(targetColSate, colDef);
    });
    //sort by orderedIndex
    updatedColDefs = _.sortBy(updatedColDefs, ["orderedIndex"]);
    this.logger.debug("updatedColDefs: ", updatedColDefs);
    return {
      custom: {
        filterModel: updatedGridOptions.custom.filterModel
      },
      columnDefs: updatedColDefs
    };
  }

  addMissingChangeableColumnDefProps(gridOptions: GridOptions) {
    if (!gridOptions.columnDefs) return gridOptions;
    gridOptions.columnDefs.map((colDef: ColDef) => {
      if (!colDef.colId) {
        colDef.colId = colDef.field;
      }
      if (colDef.sort === undefined) {
        colDef.sort = null;
      }
    });
    return gridOptions;
  }

  onFirstDataRendered(params) {
    if (this._autosizeallcol) {
      // this.grid.columnApi.autoSizeAllColumns();
      // params.api.sizeColumnsToFit();
      //only thing that seems to work
      setTimeout(() => this.grid.columnApi.autoSizeAllColumns(), 2000);
    }
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.logger.debug("aggridstylestr: ", this.aggridstylestr);
    this.logger.debug("onGridReady");
    this.logger.debug("setRowData: len: ", this.rowData.length);
    this.setRowData(this.rowData);
    this.applyCustomSavedFilters();
  }

  applyCustomSavedFilters() {
    if (
      this.mergedGridOptions.custom.filterModel &&
      !_.isEmpty(this.mergedGridOptions.custom.filterModel)
    ) {
      Object.keys(this.mergedGridOptions.custom.filterModel).map(
        (key: string) => {
          this.logger.debug("colId: ", key);
          let filterInstance: any = this.gridApi.getFilterInstance(key);
          this.logger.debug("filterInstance: ", filterInstance);
          let colFilterModel = this.mergedGridOptions.custom.filterModel[key];
          this.logger.debug("colFilterModel: ", colFilterModel);
          filterInstance.setModel(colFilterModel);
          filterInstance.onFilterChanged();
        }
      );
    }
  }

  fetchData(startRow?: number, endRow?: number): Observable<any> {
    if (!this._endpoint) return of(null);
    this._requestbody = this._requestbody || {};
    if (this._endpoint.endsWith("performFindList")) {
      if (
        this._requestbody.inputFields &&
        _.isEmpty(this._requestbody.inputFields)
      ) {
        this._requestbody.noConditionFind = "Y";
      }
      this.logger.debug(
        "this._requestbody (before defaults): ",
        this._requestbody
      );
      this._requestbody = {
        startRow: startRow || 0,
        endRow: endRow || -1,
        ...this._requestbody
      };
      this.logger.debug(
        "this._requestbody (after defaults): ",
        this._requestbody
      );
    }
    this.logger.debug("fetchData: this._requestbody: ", this._requestbody);
    this.logger.debug("fetchData: this._endpoint: ", this._endpoint);
    this.spin();
    return this.http[this._endpointverb](
      this._endpoint,
      this._requestbody
    ).pipe(
      tap((res: any) => {
        this.spin();
        if (res.list) {
          this.logger.debug(
            "internal fetch response [0-10]: ",
            _.slice(res.list, 0, 10)
          );
        }
      })
    );
  }

  callCount = 0;
  receiveGridUserPreferences(
    wrapperOptions: any,
    refresh = false
  ): Observable<any> {
    if (refresh) {
      //https://stackoverflow.com/a/57538453/536187
      this.spin();
      this.userPrefLoaded = false;
    }
    return this.http
      .post(
        `${wrapperOptions.preferenceendpointfetch}?${this.serializeForPOST(
          this.wrapperOptions,
          ["preferenceendpointfetch"]
        )}`,
        {}
      )
      .pipe(
        map(
          (res: any) => {
            this.logger.debug(
              "receiveGridUserPreferences res #",
              ++this.callCount,
              ": ",
              res
            );
            return res;
          },
          error => {
            // All recover attempts failed
            console.error("All recover attempts failed: ", error);
          }
        ),
        flatMap((prefResponse: any) => {
          //  this.logger.debug("receiveGridUserPreferences: wrapperOptions: ", this.wrapperOptions, ", res: ", prefResponse)
          // if (prefResponse.mode) {
          //   this.mode = prefResponse.mode;
          // }
          this.logger.debug("prefResponse: ", prefResponse);
          this.logger.debug(
            "datacreate (before): ",
            this._datacreate,
            ", dataupdate(before): ",
            this._dataupdate,
            ", dataremove(before): ",
            this._dataremove
          );
          if (prefResponse.datacreate && prefResponse.datacreate !== "null") {
            this._datacreate = prefResponse.datacreate;
          }
          if (prefResponse.dataupdate && prefResponse.dataupdate !== "null") {
            this._dataupdate = prefResponse.dataupdate;
          }
          if (prefResponse.dataremove && prefResponse.dataremove !== "null") {
            this._dataremove = prefResponse.dataremove;
          }
          this.logger.debug(
            "datacreate (after): ",
            this._datacreate,
            ", dataupdate(after): ",
            this._dataupdate,
            ", dataremove(after): ",
            this._dataremove
          );
          if (prefResponse.lastUpdateTime) {
            this.wrapperOptions.lastUpdateTime = prefResponse.lastUpdateTime;
          }
          if (prefResponse.appliedRequestUrl) {
            this._endpoint = prefResponse.appliedRequestUrl;
          }
          if (prefResponse.appliedRequestVerb) {
            this._endpointverb = prefResponse.appliedRequestVerb;
          }
          let requestBodyUpdated = false;
          if (prefResponse.appliedRequestBodyJSON) {
            const newRequestBody = _.defaultsDeep(
              JSON.parse(prefResponse.appliedRequestBodyJSON),
              this._requestbody
            );
            if (!_.isEqual(this._requestbody, newRequestBody)) {
              requestBodyUpdated = true;
              this._requestbody = newRequestBody;
            }
          }

          //we want to reload the grid in 2 cases
          //1. the admin prefs columnDefs have changed remotely
          //2. the user prefs columnDefs have changed remotely
          //in client mode we only reload the data if the config has changed as in 1 or 2

          let currentAdminGridOptions = this.addMissingChangeableColumnDefProps(
            prefResponse.admin
              ? _.omit(this.deserialize(prefResponse.admin), [
                  "org.apache.tomcat.util.net.secure_protocol_version"
                ])
              : {}
          );
          let currentUserGridOptions = this.addMissingChangeableColumnDefProps(
            prefResponse.user
              ? _.omit(this.deserialize(prefResponse.user), [
                  "org.apache.tomcat.util.net.secure_protocol_version"
                ])
              : {}
          );
          this.logger.debug(
            "incoming remote admin opts: ",
            currentAdminGridOptions
          );
          this.logger.debug(
            "incoming remote user opts: ",
            currentUserGridOptions
          );
          this.logger.debug("this.mergedGridOptions: ", this.mergedGridOptions);
          if (
            this.isAdminChanged(currentAdminGridOptions) ||
            this.isUserChanged(currentUserGridOptions)
          ) {
            this.userPrefLoaded = false;
            this.currentAdminGridOptions = currentAdminGridOptions;
            this.currentUserGridOptions = currentUserGridOptions;

            this.logger.debug(
              ">>>>>>>> updating to mergedGridOptions. this._gridoptions: ",
              this._gridoptions
            );
            this.mergedGridOptions = _.defaultsDeep(
              this.currentUserGridOptions,
              this.currentAdminGridOptions,
              this._gridoptions
            );
            this.editMode = this.detectColDefHasEditable(
              this.mergedGridOptions.columnDefs
            );
            if (!this.mergedGridOptions.custom) {
              this.mergedGridOptions.custom = {};
            }
            console.log("mark")
            if (
              this.editMode &&
              this.mergedGridOptions.custom.dataUniqueIdField
            ) {
              this.logger.debug(
                "setting getRowNodeId to use ",
                this.mergedGridOptions.custom.dataUniqueIdField
              );
              this.mergedGridOptions.getRowNodeId = row => {
                return row[this.mergedGridOptions.custom.dataUniqueIdField];
              };
            } else {
              throw new Error(
                `Edit mode can't be used without a custom.dataUniqueIdField set`
              );
            }
            if (refresh) {
              //https://stackoverflow.com/a/57538453/536187
              this.gridApi.setColumnDefs([]);
              this.gridApi.setColumnDefs(this.mergedGridOptions.columnDefs);
              this.applyCustomSavedFilters();
            }
            this.logger.debug(
              ">>>>>>>> this.mergedGridOptions: ",
              this.mergedGridOptions
            );

            return this.fetchData();
          } else if (requestBodyUpdated) {
            return this.fetchData();
          } else {
            return of(null);
          }
        }),
        map((datares: any) => {
          if (datares) {
            let localRowData = datares.list || datares;
            //on first load skip gridApi.setRowData, since this will be called onGridReady
            if (this.rowData) {
              this.setRowData(localRowData);
            } else {
              this.rowData = localRowData;
            }
          }
          this.userPrefLoaded = true;
          this.spin();
        })
      );
  }

  serverModeDataRetrieval() {
    let that = this;
    this.grid.api.setServerSideDatasource({
      getRows(params) {
        this.logger.debug("params.request ", params.request);
        this.logger.debug("request ", params);
        that
          .fetchData(params.request.startRow, params.request.endRow)
          .pipe(filter(e => !!e))
          .subscribe((lookupResult: any) => {
            this.logger.debug(
              "result size: " +
                lookupResult.list.length +
                ", total size: " +
                lookupResult.listSize
            );
            this.logger.debug("data sample: ", lookupResult.list[0]);
            params.successCallback(lookupResult.list, lookupResult.listSize);
          });
      }
    });
  }

  private deserialize(serializedJavascript) {
    return eval("(" + serializedJavascript + ")");
  }

  private serializeForPOST(obj, omit?: string[]) {
    if (omit) {
      obj = _.omit(obj, omit);
    }
    let str = [],
      p;
    for (p in obj) {
      let k = p,
        v = obj[p];
      if (_.isObject(v)) {
        v = JSON.stringify(v);
      }
      str.push(encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
    return str.join("&");
  }

  private isCyclic(obj: any): boolean {
    let seenObjects = [];

    function detect(obj) {
      if (obj && typeof obj === "object") {
        if (seenObjects.indexOf(obj) !== -1) {
          return true;
        }
        seenObjects.push(obj);
        for (var key in obj) {
          if (obj.hasOwnProperty(key) && detect(obj[key])) {
            console.error("cyclic structure detail:", obj, `cycle at ${key}`);
            return true;
          }
        }
      }
      return false;
    }

    return detect(obj);
  }
}
