import { Component, OnInit, AfterViewInit, ViewChild, ViewContainerRef } from "@angular/core";
import { ICellEditorAngularComp } from "ag-grid-angular";
import { IAfterGuiAttachedParams, ICellEditorParams } from "ag-grid-community";

@Component({
  selector: "date-picker",
  template: `
    <input type="date" #input [(ngModel)]="value" style="width: 100%" />
  `,
  styles: []
})
export class DatePickerComponent implements ICellEditorAngularComp, AfterViewInit {
  private params: any;
  public value: any;

  @ViewChild("input", { read: ViewContainerRef }) public input;

  ngAfterViewInit(): void {
    window.setTimeout(() => {
      this.input.element.nativeElement.focus();
    });
  }
  getValue() {
    return this.value;
  }
  // isCancelBeforeStart?(): boolean {
  //   throw new Error("Method not implemented.");
  // }
  // isCancelAfterEnd?(): boolean {
  //   throw new Error("Method not implemented.");
  // }
  // isPopup?(): boolean {
  //   throw new Error("Method not implemented.");
  // }
  // focusIn?(): void {
  //   throw new Error("Method not implemented.");
  // }
  // focusOut?(): void {
  //   throw new Error("Method not implemented.");
  // }
  agInit(params: ICellEditorParams): void {
    this.params = params;
    this.value = this.params.value;
  }
  // afterGuiAttached?(params?: IAfterGuiAttachedParams): void {
  //   throw new Error("Method not implemented.");
  // }

  constructor() {}

  ngOnInit() {}
}
