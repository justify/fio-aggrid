import { BrowserModule } from "@angular/platform-browser";
import { NgModule, Injector } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { AppComponent } from "./app.component";
import { AgGridModule } from "ag-grid-angular";
import { HttpClientModule } from "@angular/common/http";

import { createCustomElement } from "@angular/elements";
import { FioAgGridComponent } from "./fio-ag-grid-wrapper/fio-ag-grid.component";
import { ToastMessageComponent } from "./fio-ag-grid-wrapper/toast-message.component";
import { NgxSpinnerModule } from "@hardpool/ngx-spinner";
import { DatePickerComponent } from "./fio-ag-grid-wrapper/framework-components/date-picker/date-picker.component";
import { NumericEditorComponent } from "./fio-ag-grid-wrapper/framework-components/numeric-editor/numeric-editor.component";

@NgModule({
  declarations: [AppComponent, FioAgGridComponent, ToastMessageComponent, DatePickerComponent, NumericEditorComponent],
  entryComponents: [FioAgGridComponent],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgxSpinnerModule,
    AgGridModule.withComponents([DatePickerComponent, NumericEditorComponent])
  ],
  providers: [],
  // bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap() {
    // using createCustomElement from angular package it will convert angular component to stander web component
    const el = createCustomElement(FioAgGridComponent, {
      injector: this.injector
    });
    // using built in the browser to create your own custom element name
    customElements.define("fio-ag-grid", el);
  }
}
